package com.trace.traceviewdemo;

import android.os.Debug;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.io.File;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "TraceViewDemo";

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Debug.startMethodTracing(Environment.getExternalStorageDirectory().getPath() + "/" + "TraceViewDemo.trace"); /*"/data/data/com.trace.traceviewdemo/TraceViewDemo.trace");*/
        textView = (TextView) findViewById(R.id.text);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText(getMD5String());
            }
        });
    }

    private String getMD5String() {
        String res = "";
        for (int i = 0; i < 40; i++) {
            res = MD5Util.encrypt("Helloworld");
        }
        return res;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Debug.stopMethodTracing();
    }
}
